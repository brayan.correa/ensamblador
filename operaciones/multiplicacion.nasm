section .data

    mensaje1 db "Ingrese número uno",0x0A
    tamano1 equ $-mensaje1

    mensaje2 db "Ingrese número dos",10
    tamano2 equ $-mensaje2

    mensaje3 db "El resultado es",0x0A
    tamano3 equ $-mensaje3

section .bss

    numero1 resb 1 
    numero2 resb 1
    numero3 resb 1

section .text
    global _start
_start:
; primer número

;-------escribir------
    mov eax,4
    mov ebx,1
    mov ecx,mensaje1
    mov edx,tamano1
    int 80h
;-------leer-------
    mov eax,3
    mov ebx,2
    mov ecx,numero1
    mov edx,2
    int 80h

;segundo número

;-------escribir------
    mov eax,4
    mov ebx,1
    mov ecx,mensaje2
    mov edx,tamano2
    int 80h
;-------leer-------
    mov eax,3
    mov ebx,2
    mov ecx,numero2
    mov edx,2
    int 80h

;------dividir-----
    mov eax,[numero1]
    mov bl,[numero2]
    sub eax,"0"
    sub bl,"0"
    mul bl
    add al,"0"; para transformar el número a cadena se suma 0

    mov [numero3], al
;-----imprimir resultado-----
    mov eax,4
    mov ebx,1
    mov ecx,mensaje3
    mov edx,tamano3
    int 80h

    mov eax,4
    mov ebx,1
    mov ecx,numero3
    mov edx,2
    int 80h

    mov eax,1
    int 80h

    
