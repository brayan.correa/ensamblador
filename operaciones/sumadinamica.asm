;registros
;instrucciones
;directivas
%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,2 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

section .data
    mensj1 db "Ingrese numero uno",0x0A 
    ;db: reserva memoria para datos 1 byte 
    
    tamano1  equ $-mensj1
    ;equ: asigna un nombre simbólico al valor de una expresión.

    mensj2 db "Ingrese numero dos",0x0A 
    tamano2  equ $-mensj2

    mensaje db "El resultado es:",0x0A 
    len  equ $-mensaje

section .bss
    num1 resb 1
     	;deja un espacio de 1 bytes sin inicializar
    num2 resb 1
    suma resb 1
    
section .text   
    global _start
_start:
;----dato 1-------
    escribir mensj1, tamano1
    leer num1,2
;----dato 2-------
    escribir mensj2, tamano2
    leer num2,2
;------suma-----------
    mov eax, [num1]
    mov ebx, [num2]
    sub eax, "0"
    sub ebx, "0"
    add eax,ebx
    add eax,"0";Para transformar de número a cadena se suma "0"    ;de cadena a numero se resta "0"

    mov [suma], eax
;---------imprime la suma----------
    escribir mensaje, len

    mov eax, 4
    mov ebx, 1
    mov ecx, suma
    mov edx, 1
    int 80h

    mov eax, 1
    int 80h
    
    