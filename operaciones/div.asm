;registros
;instrucciones
;directivas
%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

section .data

    mensaje1 db "Ingrese número uno",0x0A
    tamano1 equ $-mensaje1

    mensaje2 db "Ingrese número dos",10
    tamano2 equ $-mensaje2

    mensaje3 db "El cociente ",0x0A
    tamano3 equ $-mensaje3

    mensaje4 db "El residuo ",0x0A
    tamano4 equ $-mensaje4

section .bss

    numero1 resb 1 
    numero2 resb 1
    numero3 resb 1
    numero4 resb 1

section .text
    global _start
_start:
; primer número

;-------escribir------
escribir mensaje1, tamano1
;-------leer-------
    mov eax,3
    mov ebx,2
    mov ecx,numero1
    mov edx,2
    int 80h

;segundo número

;-------escribir------
escribir mensaje2, tamano2
;-------leer-------
    mov eax,3
    mov ebx,2
    mov ecx,numero2
    mov edx,2
    int 80h

;------dividir-----
    mov eax,[numero1]
    mov bl,[numero2]
    sub eax,"0"
    sub bl,"0"
    div bl
    add al,"0"; para transformar el número a cadena se suma 0
    mov [numero3], al
    mov ah,"0"
    mov [numero4], ah


;-----imprimir resultado-----
escribir mensaje3, tamano3
    mov eax,4
    mov ebx,1
    mov ecx,numero3
    mov edx,1
    int 80h

   
escribir mensaje4, tamano4

    mov eax,4
    mov ebx,1
    mov ecx,numero4
    mov edx,1
    int 80h

    mov eax,1
    int 80h

    
