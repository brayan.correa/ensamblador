%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,2 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro
section .data
    msj db 10,"Carpeta o directorio creado",10
    len_msj equ $ - msj

    msj2 db "Ingrese la ruta del path",10
    len_msj2  equ $-msj2

    ;path db "/home/brayan/Documentos/directorio",0
    ;len_path equ $ - path

section .bss
    path resb 2


section .text
    global _start
_start:


escribir msj2, len_msj2
   leer path , 35

mov eax, 39 ;servicio para crear un directorio
mov ebx, path  ;define la ruta del servicio
mov ecx, 0x1FF ; definimos el permiso 777
int 80h

escribir msj, len_msj

mov eax, 1
int 80h
