%macro escribir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

%macro writefile 2-3+
  %ifstr %2
    jmp     %%endstr
    %if %0 = 3
      %%str:    db      %2,%3
    %else
      %%str:    db      %2
    %endif
    %%endstr: mov     dx,%%str
            mov     cx,%%endstr-%%str
  %else
    mov     dx,%2
    mov     cx,%3
  %endif
    mov     bx,%1
    mov     ah,0x40
    int     0x21
%endmacro

section .data
    Suma db ' + '
    Igual db ' = '

    msj_arreglo db 10,'Arreglo: '
	len_arreglo equ $-msj_arreglo

    archivoLectura db '/home/brayan/Documentos/ensamblador/Pruebafinal/Correa_Brayan.txt',0
    newLine db '',10

    mjs_repite db ' se repite '
    len_repite equ $ -mjs_repite

    mjs_veces db ' veces',10
    len_veces equ $ -mjs_veces

    mjs_sumuma db 'La suma de todos los números és: '
    len_sumuma equ $ -mjs_sumuma

    mjs_Error db 10,'Error en el archivo'
    len_Error equ $-mjs_Error
    
    arreglo db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    len_arreglo_arreglo equ $ -arreglo
    
    suma db '  '
    nuevaLinea db 10,' '

section .bss
    texto resb 50		;contenido del archivo
	idarchivo resd 1 
    aux2 resb 1
    aux resb 1
    sum resb 2
section .text
    global _start
_start:

    ;-------------------abrir el archivo para leer-------------------
	mov eax, 5		;5 lectura de archivo
	mov ebx, archivoLectura	;ruta
	mov ecx,0 		;modo de acceso lectura
	mov edx,0 		;permite leer si esta creado
	int 80h 
	test eax,eax		;hace un test
	jz salir
	mov dword[idarchivo], eax
	escribir msj_arreglo, len_arreglo
	mov eax,3		    ;servicio 3 lectura	
	mov ebx,[idarchivo]	;unidad de entrada
	mov ecx,arreglo
	mov edx,50
	int 80h
	escribir arreglo , 50
    escribir nuevaLinea,1
	mov eax,6		;6 cerrar el archivo
	mov ebx,[idarchivo]
	mov ecx,0
	mov edx,0
	int 80h
;-------------------cierra el archivo-------------------

;-----------------------recorrido---------------------------
    mov esi, arreglo
    mov edi, 0
    mov eax, [esi]
    mov [aux2], eax
    mov bl, 0
    mov [aux], bl
    mov cl, 0
    mov dl, 0
numeroRepetido:
    mov al, [aux2]
    mov bl, [esi]
    call task1
    inc esi
    inc edi
    cmp edi, len_arreglo_arreglo
    jb numeroRepetido
    jmp arregloC
task1:
    cmp al, bl
    je task2
    ret
task2:
    inc cl
    ret
arregloC:
    inc dl
    mov al, dl
    mov esi, arreglo
    mov edi, 0
    call I
    push edx
    call presentarR
    pop edx
    mov al, [esi]
    mov [aux2], al
    mov cl, 0
    cmp dl, len_arreglo_arreglo
    jb numeroRepetido
    jmp F
I:
    inc esi
    inc edi
    dec al
    cmp al, 0
    jg I
    ret
presentarR:
    add cl, '0'
    mov [aux], cl
    escribir aux2, 1
    escribir mjs_repite, len_repite
    escribir aux, 1
    escribir mjs_veces, len_veces
    ret

F:
    mov esi, 0
    mov ecx, len_arreglo_arreglo
    mov bl, 0
    clc 
sumMM:
    mov al, [arreglo+esi]
    sub al, '0'
    add bl, al
    inc esi
    loop sumMM
    mov al, bl
    mov bl, 10
    div bl
    add ax,'00'
    mov [sum], ax
    escribir mjs_sumuma, len_sumuma
    escribir sum, 2
    jmp salir
errordeArchivo:
    escribir mjs_Error, len_Error
salir:

    mov eax, 1
    int 80h