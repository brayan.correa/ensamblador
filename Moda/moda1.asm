%macro escribir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

section .data
	arreglo db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	len_arreglo equ $-arreglo
	
section .bss
	resultado resb 2 
    aux resb 1	

section .text
    global _start
_start:

    mov esi, arreglo
    mov edi, 0
    mov al, [esi]
    mov [aux], al

    escribir aux , 1
    mov cl, 0

salir:
	mov eax, 1
	int 80h