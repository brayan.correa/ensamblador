%macro escribir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 2
    mov eax,3
    mov ebx,2
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    msj1 db "Ingrese un número!", 0xa, 0x0
    len_msj1 equ $-msj1
    msj2 db "El nùmero es primo", 0xa, 0x0
    len_msj2 equ $-msj2
    msj3 db "El número no es primo", 0xa, 0x0
    len_msj3 equ $-msj3
section .bss
	num1 resb 1
section .text
	global _start:
_start:
escribir msj1,len_msj1
leer num1,2
mov cl,2
task1:
mov al,[num1]
sub al,"0"
cmp al,cl
jz primo
jmp task2
;2-3-5-7
task2:
mov ah,0
mov al,[num1]
sub al,"0"
div cl
inc cl
cmp ah,0
jz noPrimo
jmp task1
primo:
escribir msj2,len_msj2
jmp salir
noPrimo:
escribir msj3,len_msj3
jmp salir
salir:
	mov eax, 1
	int 21h
