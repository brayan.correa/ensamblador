%macro escribir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 2
    mov eax,3
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro


section .data
    msj1 db "Ingrese número uno",10
    len_msj1 equ $-msj1

    msj2 db "Ingrese número dos"
    len_msj2 equ $-msj2

    msj3 db "La resta es",10
    len_msj3 equ $-msj3

section .bss
    num1 resb 1
    num2 resb 1
    resta resb 1

section .text
    global _start
_start:

escribir msj1, len_msj1
leer num1,2

escribir msj2,len_msj2
leer num2,2

mov eax,[num1]
mov ebx,[num2]
sub edx,"0"
sub ecx,"0"
sub eax,ebx
add eax,"0"
mov[resta],eax

escribir msj3, len_msj3

mov eax,4
mov ebx,1
mov ecx,resta
mov edx,1
int 80h


mov eax,1
int 80h                     