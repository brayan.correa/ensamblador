%macro escribir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 2
    mov eax,3
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

section .data
    msj1 db "Ingresar número uno",10
    len_msj1 equ $-msj1

    msj2 db "Ingresar número dos",10
    len_msj2 equ $-msj2

    msj3 db "negativo",10
    len_msj3 equ $-msj3

    msj4 db "positivo",10
    len_msj4 equ $-msj4


section .bss
    num1 resb 2
    num2 resb 2

section .text
    global _start
_start:

    escribir msj1,len_msj1
    leer num1,2
    escribir msj2,len_msj2
    leer num2,2

    mov ax,[num1]
    mov bx,[num2]
    sub ax,"0"
    sub bx,"0"
    sub ax,bx
    ;add eax,"0"
    js negativo
    jmp positivo

negativo:
    escribir msj3,len_msj3
    jmp salir
positivo:
    escribir msj4,len_msj4
    jmp salir

salir:
    mov eax,1
    int 80h
