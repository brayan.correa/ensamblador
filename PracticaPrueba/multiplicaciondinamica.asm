%macro escribir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 2
    mov eax,3
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

section .data
    msj1 db "Ingresar número uno",10
    len_msj1 equ $-msj1

    msj2 db "Ingrese número dos",10
    len_msj2 equ $-msj2

    msj3 db "El resultado es",10
    len_msj3 equ $-msj3

section .bss
    num1 resb 1
    num2 resb 1
    multiplicacion resb 1

section .text
    global _start
_start:

escribir msj1,len_msj1
leer num1,2

escribir msj2,len_msj2
leer num2,2

mov ax,[num1]
mov bx,[num2]
sub ax,"0"
sub bx,"0"
mul bx
add al,"0"
mov[multiplicacion],al

escribir msj3,len_msj3

mov eax,4
mov ebx,1
mov ecx,multiplicacion
mov edx,1
int 80h

mov eax,1
int 80h

