%macro escribir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

section .data
    msj1 db "Ingrese primer número a comparar",10
    len_msj1 equ $-msj1

    msj2 db "Ingrese el segundo número a comparar",10
    len_msj2 equ $-msj2

    msj3 db "El numero es mayor",10
    len_msj3 equ $-msj3

    msj4 db "El número es menor",10
    len_msj4 equ $-msj4

section .bss
    num1 resb 1
    num2 resb 1

section .text
    global _start
_start:

escribir msj1,len_msj1
leer num1,2

escribir msj2,len_msj2
leer num2,2

mov al,[num1]
mov bl,[num2]
cmp al,bl
jg mayor
jmp menor

mayor:
    escribir msj3,len_msj3
    jmp salir
menor:
    escribir msj4,len_msj4
    jmp salir
salir:
    mov eax,1
    int 80h