; A prime number is an integer that is divisible only by 1 and by itself.
; A program that takes a number n as input, and prints back to the console all the    primes that are larger than 1 but not larger than n.



; ===============================================


start:
       ; Your program begins here:

       ; Division Operations - 
       ;eax <- edx:eax/arg
       ;edx <- edx:eax % arg

       call    read_hex

lb2:    
       dec     eax
       jz      exit
       jmp     prime_check

prime_check:

       ; The check for whether 2 is a prime is done separately here
       mov     ecx, eax
       sub     ecx, 2
       jz      is_prime

       mov     ecx, eax        ; eax will be overwritten, hence need to store it someplace else to recover it
       mov     edx, 0          ; initialing the edx:eax registers
       mov     ebx, 2

lb1:
       dec     eax             ; The 3 lines of code are there cause I need a way to exit the program, when eax inside this block is 1.
       jz      exit            ; This seems to be an easy fix. Although downright inelegant
       inc     eax
       div     ebx             
       mov     eax, ecx
       sub     edx, 1
       jc      lb2             ; The case where number isnt prime
       mov     eax, ecx        ; re-initialing the edx:eax registers
       mov     edx, 0          ; re-initialing the edx:eax registers
       inc     ebx
       sub     ecx, ebx
       jz      is_prime
       mov     ecx, eax    
       jmp     lb1

is_prime:
       call    print_eax
       jmp     lb2

exit:
       ; Exit the process:
       push    0
       call    [ExitProcess]

