section .data

	msj db "Ingrese un número:",10
	len equ $ - msj

section .bss

   numero  resb 2

section .text
    global _start
_start:
;loop: para repetir un ciclo hasta que cx sea = 0
    mov eax, 4
	mov ebx, 1
	mov ecx, msj
	mov edx, len
	int 80h

    mov cx, [numero]

repetir:
    mov ax, 2
    mov dx,cx
    add dx,"0"
    int 80h
    loop repetir


    mov eax, 4
	mov ebx, 1
	mov ecx, numero
	mov edx, 1
	int 80h
    

salir:
    mov eax, 1
    int 80h

