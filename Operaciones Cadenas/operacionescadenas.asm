section .data
    numero1 db "3277"
    numero2 db "2566"
    resultado db "     "
    len_resultado equ $- resultado
section .text
    global _start
_start:
    mov esi, 3 
    mov ecx, 4
    clc ;instrucciones sin operandos para desactivar el carri; es decir desactivar el bit (estado activo) de la bandera carry
procesosuma:
    mov al, [numero1+esi]
    ;mov ah, [numero2+esi]
    ;adc al,ah

    ;#####operacion de suma######
    adc al, [numero1+esi]
    aaa ; si al cuya operacion al + [numero2 +esi]
    pushf
    ;aas no afecta a al
    or al, 30h ; se convierte en aas ;or trabaja con vinarios; 30h es el ajuste en el bcd
    popf
    mov [resultado+esi],al

    dec esi
    loop procesosuma

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len_resultado
    int 80h



salir: 
    mov eax, 1
    int 80h

