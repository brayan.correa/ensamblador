%macro escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
segment .data
    msj1 db "Ingrese número",10
    len_msj1 equ $-msj1

     newLine db '',10

      espace db ' '
segment .bss
    numero resb 1
segment .text
    global _start
_start:
    escribir msj1, len_msj1
    leer numero,2
    mov al, [numero]
    sub al, "0"
    cmp al, 1
    mov esi, 0
task1:
    mov bl, 2
    div bl
    push eax
        escribir espace,1
    pop ebx
    push ebx
    mov al, bl
    inc esi
    cmp al,1
    je task2
    jmp task1


task2:
    push eax
    escribir newLine,1
    pop eax
    add al, "0"
    mov [numero], al
    escribir numero,1

    mov ecx, esi


task3:
    pop eax
    push ecx
    add ah, "0"
    mov [numero],ah
    escribir numero,1
    pop ecx
    loop task3
    jmp salir

task4:
    escribir numero,1

salir:
    mov eax,1
    int 80h