
global _start      

section .text

_start:

    mov eax, 4        
    mov ebx, 1      
    mov ecx, message    
    mov edx, length      
    int 80h
    
    mov eax, 4        
    mov ebx, 1      
    mov ecx, message1   
    mov edx, length1      
    int 80h


    mov eax, 4        
    mov ebx, 1      
    mov ecx, message2    
    mov edx, length2      
    int 80h


    mov eax, 4        
    mov ebx, 1      
    mov ecx, message3    
    mov edx, length3      
    int 80h


    ; sys_exit(return_code)

    mov eax, 1       
   

    int 80h

 

section .data

    message: db 'Brayan' 
    length: equ $-message  

    message1: db ' Eduardo ' 
    length1: equ $-message1  

    message2: db 'Correa ', 0x0A
    length2: equ $-message2

    message3: db 'Mogro ....................'
    length3: equ $-message3   
