;número de caracteres que se pueden modificar
;byte  1
;word  2
;dword 4
;qword  8
;tbyte  10
section .data
    mensaje db "desplegar los asteriscos 9 veces",0x0A 
    len  equ $-mensaje
    
section .text   
    global _start
_start:
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len
    int 80h

    mov [mensaje], dword "hola"
    
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len
    int 80h

    mov [mensaje + 15], dword "****"
    
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len
    int 80h

    mov eax, 1
    int 80h
    