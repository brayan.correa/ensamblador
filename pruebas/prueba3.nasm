section .data
    msj db "desplegar los asteriscos 9 veces"
    len  equ $-msj
    asterisco times 9 db 0x0A,"*"
section .text   
    global _start
_start:
    mov eax, 4
    mov ebx, 1
    mov ecx, msj
    mov edx, len
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, asterisco
    mov edx, 9
    int 80h
    mov eax, 1
    int 80h
    