section .data
 
   ; Mensajes
 
   msg1      db      10,'-Áreas: Elija una opción-',10,0
   lmsg1      equ      $ - msg1


 
    msg2      db      10,'ingrese el valor del lado 1: ',0
    lmsg2     equ      $ - msg2
    msg3      db      'Ingrese el valor del lado 2: ',0
    lmsg3     equ      $ - msg3

 
   msg4       db      10,'1. Ingrese el valor de la base',10,0
   lmsg4      equ      $ - msg4
   msg5       db      '2. ingrese el valor de la altura',10,0
   lmsg5      equ      $ - msg5

   

 
   msg6       db      '3. Ingrese el valor de pi en este caso 3',10,0
   lmsg6      equ      $ - msg6
   msg7       db      '4. Ingrese el valor del radio elevado al cuadrado',10,0
   lmsg7      equ      $ - msg7
   
 

  


   msg8       db      '1. Area cuadrado ',10,0
   lmsg8      equ      $ - msg8
   msg9       db      '2. Area rectangulo ',10,0
   lmsg9      equ      $ - msg9
   msg10       db      '3. Area circulo ',10,0
   lmsg10      equ      $ - msg10


   msgopciones       db      'Opciones: ',10,0
   lmsgopciones      equ      $ - msgopciones



 
   msg11      db      10,'El area es: ',0
   lmsg11      equ      $ - msg11
 
   msg12      db      10,'Opcion Incorrecta',10,0
   lmsg12      equ      $ - msg12
 
   nlinea      db      10,10,0
   lnlinea      equ      $ - nlinea
 
section .bss
 
   ; Espacios reservados para almacenar los valores proporcionados
   ; por el usuario.
 
   opc        resb    2
   lado1      resw    2
   lado2      resw    2
   base       resw    2
   altura     resw    2
   pi         resw    2
   radio      resw    2
   expo       resw    2
   
   result     resw    2
 
section .text
 
   global _start
 
_start:
 
   ; Imprimimos en pantalla el mensaje 1
   mov eax, 4
   mov ebx, 1
   mov ecx, msg1
   mov edx, lmsg1
   int 80h
 


 ; Imprimimos en pantalla el mensaje cuadrado
   mov eax, 4
   mov ebx, 1
   mov ecx, msg8
   mov edx, lmsg8
   int 80h

    ; Imprimimos en pantalla el mensaje rectangulo
   mov eax, 4
   mov ebx, 1
   mov ecx, msg9
   mov edx, lmsg9
   int 80h

      ; Imprimimos en pantalla el mensaje circulo
   mov eax, 4
   mov ebx, 1
   mov ecx, msg10
   mov edx, lmsg10
   int 80h



 
   ; Imprimimos en pantalla el mensaje 8
   mov eax, 4
   mov ebx, 1
   mov ecx, msgopciones
   mov edx, lmsgopciones
   int 80h
   
   ; Obtenemos la opcion deseada por el usuario
   mov ebx,0
   mov ecx,opc
   mov edx,2
   mov eax,3
   int 80h
 
   mov ah, [opc]   ; Movemos la opcion seleccionada al registro ah
   sub ah, '0'      ; Convertimos de ascii a decimal
 
   ; Comparamos el valor ingresado por el usuario para saber que
   ; operacion desea realizar.
   cmp ah, 1
   je cuadrado
   cmp ah, 2
   je rectangulo
   cmp ah, 3
   je circulo
   
 
   ; Si el valor ingresado por el usuario no cumple ninguna de las
   ; condiciones anteriores entonces mostramos un mensaje de error y
   ; finalizamos el programa.
   mov eax, 4
   mov ebx, 1
   mov ecx, msg12
   mov edx, lmsg12
   int 80h
 
   jmp salir
 
cuadrado:

 ; Imprimimos en pantalla el mensaje 2
   mov eax, 4
   mov ebx, 1
   mov ecx, msg2
   mov edx, lmsg2
   int 80h
 
   ; Obtenemos el valor de lado1
   mov eax, 3
   mov ebx, 0
   mov ecx, lado1
   mov edx, 2
   int 80h

 ; Imprimimos en pantalla el mensaje 3
   mov eax, 4
   mov ebx, 1
   mov ecx, msg2
   mov edx, lmsg2
   int 80h
 
   ; Obtenemos el valor de lado2
   mov eax, 3
   mov ebx, 0
   mov ecx, lado2
   mov edx, 2
   int 80h


   ; Guardamos los numeros en los registros eax y ebx
   mov eax, [lado1]
   mov ebx, [lado2]
 
   ; Conversion de ascii a decimal
   sub ax, '0'
   sub bx, '0'
 
   ; Multiplicacion. AL = AX x BX
   mul bx
 
   ; Conversion decimal a ascii
   add al, '0'
 
   ; Movemos el resultado
   mov [result], al
 
   ; Imprimimos el mensaje 9
   mov eax, 4
   mov ebx, 1
   mov ecx, msg11
   mov edx, lmsg11
   int 80h
 
   ; Imprimimos el resultado
   mov eax, 4
   mov ebx, 1
   mov ecx, result
   mov edx, 1
   int 80h
 
   ; Finalizamos el programa
   jmp salir
 
rectangulo:

 ; Imprimimos en pantalla el mensaje 2
   mov eax, 4
   mov ebx, 1
   mov ecx, msg4
   mov edx, lmsg4
   int 80h
 
   ; Obtenemos el valor de lado1
   mov eax, 3
   mov ebx, 0
   mov ecx, base
   mov edx, 2
   int 80h

 ; Imprimimos en pantalla el mensaje 3
   mov eax, 4
   mov ebx, 1
   mov ecx, msg5
   mov edx, lmsg5
   int 80h
 
   ; Obtenemos el valor de lado2
   mov eax, 3
   mov ebx, 0
   mov ecx, altura
   mov edx, 2
   int 80h


   ; Guardamos los numeros en los registros eax y ebx
   mov eax, [base]
   mov ebx, [altura]
 
   ; Conversion de ascii a decimal
   sub ax, '0'
   sub bx, '0'
 
   ; Multiplicacion. AL = AX x BX
   mul bx
 
   ; Conversion decimal a ascii
   add al, '0'
 
   ; Movemos el resultado
   mov [result], al
 
   ; Imprimimos el mensaje 9
   mov eax, 4
   mov ebx, 1
   mov ecx, msg11
   mov edx, lmsg11
   int 80h
 
   ; Imprimimos el resultado
   mov eax, 4
   mov ebx, 1
   mov ecx, result
   mov edx, 1
   int 80h
 
   ; Finalizamos el programa
   jmp salir
 
 circulo:


  ; Imprimimos en pantalla el mensaje
   mov eax, 4
   mov ebx, 1
   mov ecx, msg6
   mov edx, lmsg6
   int 80h

   ; Obtenemos el valor de lado1
   mov eax, 3
   mov ebx, 0
   mov ecx, pi
   mov edx, 2
   int 80h




 ; Imprimimos en pantalla el mensaje 
   mov eax, 4
   mov ebx, 1
   mov ecx, msg7
   mov edx, lmsg7
   int 80h
 
   ; Obtenemos el valor de lado2
   mov eax, 3
   mov ebx, 0
   mov ecx, radio
   mov edx, 2
   int 80h



   ; Guardamos los numeros en los registros eax y ebx
   mov eax, [radio]
   mov ebx, [pi]

 
   ; Conversion de ascii a decimal
   sub ax, '0'
   sub bx, '0'
 
   ; Multiplicacion. AL = AX x BX
   mul bx
 
   ; Conversion decimal a ascii
   add al, '0'
 
   ; Movemos el resultado
   mov [result], al
 
   ; Imprimimos el mensaje 9
   mov eax, 4
   mov ebx, 1
   mov ecx, msg11
   mov edx, lmsg11
   int 80h
 
   ; Imprimimos el resultado
   mov eax, 4
   mov ebx, 1
   mov ecx, result
   mov edx, 1
   int 80h
 
   ; Finalizamos el programa
   jmp salir
 

 
salir:
   ; Imprimimos dos nuevas lineas
   mov eax, 4
   mov ebx, 1
   mov ecx, nlinea
   mov edx, lnlinea
   int 80h
   ; Finalizamos el programa
   mov eax, 1
   mov ebx, 0
   int 80h 