section .data
	msg1 db "Ingrese 6 números y presione enter:", 0xA
	len1 equ $-msg1	

	arreglo db 0,0,0,0,0,0
	len_arreglo equ $-arreglo

	msj_resultado db "El mayor número es: ",
	len_resultado equ $-msj_resultado


	msj_resultado_menor db "El menor número es: ",
	len_resultado_menor equ $-msj_resultado_menor

   
	new_line db '',10

section .bss
	dato resb 2

section .text
	global _start

_start:

	mov esi, arreglo
	mov edi, 0

	mov eax, 4
	mov ebx, 1
	mov ecx, msg1
	mov edx, len1
	int 0x80

leer:
	mov eax, 3
	mov ebx, 0
	mov ecx, dato
	mov edx, 2
	int 80h

	mov al, [dato]
	sub al, '0'		
	
	mov [esi] , al
	
	add edi, 1     		
	add esi, 1

	cmp edi, len_arreglo
	jb leer

    mov esi, arreglo
    mov edi, 0

imprimir2:
	mov al, [esi]
	add al,'0'
    mov [dato], al


	mov eax, 4
	mov ebx, 1
	mov ecx, dato
	mov edx, 1
	int 80h;

    add esi, 1
    add edi ,1
    cmp edi, len_arreglo
    jb imprimir2

    mov eax, 4
	mov ebx, 1
	mov ecx, new_line
	mov edx, 1
	int 80h

    mov ecx, 0
	mov bl, 0


comparacion:
	mov al,[arreglo + ecx] 
	cmp al, bl
	jb contador
	mov bl, al
    

contador:
	inc ecx
	cmp ecx, len_arreglo
	jb comparacion




imprimir:
	add bl, '0'
	mov [dato], bl

	mov eax, 4
	mov ebx, 1
	mov ecx, msj_resultado
	mov edx, len_resultado
	int 80h;

	mov eax, 4
	mov ebx, 1
	mov ecx, dato
	mov edx, 1
	int 80h;

    mov eax, 4
	mov ebx, 1
	mov ecx, new_line
	mov edx, 1
	int 80h

imprimirMenor:
	add al, '0'
	mov [dato], al

	mov eax, 4
	mov ebx, 1
	mov ecx, msj_resultado_menor
	mov edx, len_resultado_menor
	int 80h;

	mov eax, 4
	mov ebx, 1
	mov ecx, dato
	mov edx, 1
	int 80h;

salir:
	mov eax, 1
	int 0x80	