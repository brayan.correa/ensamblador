;registros
;instrucciones
;directivas
%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,2 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

section .data
    msj1 db "Ingresar un número",0x0A 
    len_msj1  equ $-msj1

section .bss
    num1 resb 1

section .text   
    global _start
_start:         

    escribir msj1,len_msj1
    leer num1,1


    jmp principal

principal:
    cmp ecx,0
    jz salir
    jmp imprimir

imprimir:

    mov ecx,[num1]
    sub ecx,"0"
    dec ecx
    mov eax,ecx
    push ecx
    add ecx,"0"
    mov [num1],ecx

    mov eax, 4
    mov ebx, 1
    mov ecx, num1
    mov edx, 1
    int 80h

    pop ecx
    jmp principal
    

salir:
    mov eax, 1
    int 80h