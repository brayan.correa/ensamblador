;registros
;instrucciones
;directivas
%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,2 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

section .data


    msj2 db "*",0x0A 
    len_msj2  equ $-msj2
    msj3 db " ",0x0A 
    len_msj3  equ $-msj3

section .bss

section .text   
    global _start
_start:       
    mov eax,5  
    mov ecx,5
    jmp principal

principal:
    cmp eax,0
    cmp ecx,0
    jz salir
    jmp imprimirColumna

imprimirColumna:

    dec eax
    dec ecx
    
    push eax
    push ecx
    
    mov eax, 4
    mov ebx, 1
    mov ecx, msj2
    mov edx, 1
    int 80h

    pop eax
    pop ecx
    jz imprimirFila
    jmp principal

imprimirFila:


    dec eax
    dec ecx

    push eax
    push ecx
    
    mov eax, 4
    mov ebx, 1
    mov ecx, msj3
    mov edx, 1
    int 80h

    pop eax
    pop ecx
    jmp imprimirColumna
    jmp salir   
    

salir:
    mov eax, 1
    int 80h