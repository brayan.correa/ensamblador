extern printf, scanf

section .data
ingresar db 'Ingrese un número!', 0xa, 0x0
sis db 'C es un número primo', 0xa, 0x0
no db 'No es un número primo', 0xa, 0x0
format_d db '%d', 0x0

section .text
global _start
_start:

es_primo:
;Inserte el código aquí!


main:
push ebp
mov ebp, esp

;Dejamos espacio para un entero en la pila
;El que ingresaremos con scanf
sub esp, 4

;Frase de bienvenida
push ingresar
call printf
add esp, 4

;Solicitamos al usuario ingresar un número
push esp ;Dirección del entero
push format_d ; %d
call scanf
add esp, 8

;Llamamos a la función con el entero ingresado
push dword [esp]
call es_primo
;Probamos el retorno (eax == 0 ?)
test eax, eax
;Si es igual a cero => no es primo
jz noPrimo
;Si no
push sis
call printf
;Vamos al final de la función para no
;ingresar en la sección noPrimo
jmp quit

noPrimo:
push no
call printf

quit:
leave
ret