; un programa que muestra la funcionalidad loop
; e imprimir el resultado en pantalla

section .data
	msj db "Ingrese un número:",10
	len equ $ - msj
    msj2 db "Brayan:",10
	len2 equ $ - msj2
	new_line db ' ',10

section .bss
	numero resb 2
	
section .text
	global _start

_start :
	; ******* mensaje ingrese número
	mov eax, 4
	mov ebx, 1
	mov ecx, msj
	mov edx, len
	int 80h

	mov eax, 3
	mov ebx, 2
	mov ecx, numero
	mov edx, 1
	int 80h

	mov ecx, [numero]
	sub ecx, '0'	
	jmp imprimir

imprimir:
	push ecx		; en numero
	mov eax, ecx
	add eax, '0'
	mov [numero], eax

	mov eax, 4
	mov ebx, 1
	mov ecx, numero
	mov edx, 1
	int 80h

	mov eax, 4
	mov ebx, 1
	push ecx		;se envia la referencia de ecx a pila
	mov ecx, msj2
	mov edx, len2
	int 80h
	
	pop ecx
	loop imprimir			; en este instante se decrementa cx en 1


	mov eax, 4
	mov ebx, 2
	mov ecx, new_line
	mov edx, 1
	int 80h

	pop ecx
	loop imprimir
	jmp salir
salir:
	mov eax, 1
	int 80h