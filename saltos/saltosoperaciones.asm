section .data
 
   ; Mensajes
 
   msg1      db      10,'-Calculadora-',10,0
   lmsg1      equ      $ - msg1
 
   msg2      db      10,'Numero 1: ',0
   lmsg2      equ      $ - msg2
 
   msg3      db      'Numero 2: ',0
   lmsg3      equ      $ - msg3
 
   msg4      db      10,'1. Sumar',10,0
   lmsg4      equ      $ - msg4
 
   msg5      db      '2. Restar',10,0
   lmsg5      equ      $ - msg5
 
   msg6      db      '3. Multiplicar',10,0
   lmsg6      equ      $ - msg6
 
   msg7      db      '4. Dividir',10,0
   lmsg7      equ      $ - msg7
 
   msg8      db      'Operacion: ',0
   lmsg8      equ      $ - msg8
 
   msg9      db      10,'Resultado: ',0
   lmsg9      equ      $ - msg9
 
   msg10      db      10,'Opcion Incorrecta',10,0
   lmsg10      equ      $ - msg10
 
   nlinea      db      10,10,0
   lnlinea      equ      $ - nlinea
 
section .bss
 
   ; Espacios reservados para almacenar los valores proporcionados
   ; por el usuario.
 
   opc       resb    2
   num1      resw    2
   num2      resw    2
   result    resw    2
 
section .text
 
   global _start
 
_start:
 
   ; Imprimimos en pantalla el mensaje 1
   mov eax, 4
   mov ebx, 1
   mov ecx, msg1
   mov edx, lmsg1
   int 80h
 
   ; Imprimimos en pantalla el mensaje 2
   mov eax, 4
   mov ebx, 1
   mov ecx, msg2
   mov edx, lmsg2
   int 80h
 
   ; Obtenemos el valor de numero1
   mov eax, 3
   mov ebx, 0
   mov ecx, num1
   mov edx, 2
   int 80h
 
   ; Imprimimos en pantalla el mensaje 3
   mov eax, 4
   mov ebx, 1
   mov ecx, msg3
   mov edx, lmsg3
   int 80h
 
   ; Obtenemos el valor de numero2
   mov eax, 3
   mov ebx, 0
   mov ecx, num2
   mov edx, 2
   int 80h
 



 
   
 
sumar:
   ; Guardamos los numeros en los registros eax y ebx
   mov eax, [num1]
   mov ebx, [num2]
 
   ; Conversion de ascii a decimal
   sub eax, '0'
   sub ebx, '0'
 
   ; Suma
   add eax, ebx
 
   ; Conversion de decimal a ascii
   add eax, '0'
 
   ; Movemos el resultado
   mov [result], eax
 
   ; Imprimimos el mensaje 9
   mov eax, 4
   mov ebx, 1
   mov ecx, msg9
   mov edx, lmsg9
   int 80h
 
   ; Imprimimos el resultado
   mov eax, 4
   mov ebx, 1
   mov ecx, result
   mov edx, 1
   int 80h
 
   ; Finalizamos el programa
   jmp restar
 
restar:
   ; Guardamos los numeros en los registros eax y ebx
   mov eax, [num1]
   mov ebx, [num2]
 
   ; Conversion de ascii a decimal
   sub eax, '0'
   sub ebx, '0'
 
   ; Resta
   sub eax, ebx
 
   ; Conversion de decimal a ascii
   add eax, '0'
 
   ; Movemos el resultado
   mov [result], eax
 
   ; Imprimimos el mensaje 9
   mov eax, 4
   mov ebx, 1
   mov ecx, msg9
   mov edx, lmsg9
   int 80h
 
   ; Imprimimos el resultado
   mov eax, 4
   mov ebx, 1
   mov ecx, result
   mov edx, 1
   int 80h
 
   ; Finalizamos el programa
   jmp sumar
 

 

 

