;nasm 2.11.08
%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro


section .data
    msj1:     db 'Numero mayor',10    ;
    len1:  equ $-msj1 
    msj2:     db 'Numero menor',10    ; 
    len2:  equ $-msj2            
    
    msj3 db "Ingrese numero uno",0x0A 
    len3:  equ $-msj3 
    msj4 db "Ingrese numero dos",0x0A 
    len4:  equ $-msj4
    
 section .bss
    num1 resb 1
    num2 resb 1

section .text
	global _start

_start:
;----número 1-------
    escribir msj3, len3
    leer num1,2
;----número 2-------
    escribir msj4, len4
    leer num2,2


	mov al,[num1]           
	mov bl,[num2]  
    cmp al,bl
    jg mayor
    jmp menor
    
  mayor:
    mov eax,4       
	mov ebx,1
    mov ecx,msj1
    mov edx,len1                
	int 80h     
    jmp salir
    
   menor:
    mov eax,4       
	mov ebx,1
    mov ecx,msj2
    mov edx,len2                
	int 80h    
    
  salir:
	mov eax,1                     
	int 80h;