%macro escribir 2
    mov eax,4 
    mov ebx,1 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,2 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

section .data
    msj1 db "Ingrese numero uno",0x0A  
    len1  equ $-msj1
    msj2 db "Ingrese numero dos",0x0A 
    len2  equ $-msj2
    msj_negativo db "es negativo",0x0A 
    len_negativo equ $-msj_negativo
    msj_positivo db "es positivo",0x0A 
    len_positivo equ $-msj_positivo

section .bss
    num1 resb 2
    num2 resb 2

section .text   
    global _start
_start:
;----dato 1-------
    escribir msj1, len1
    leer num1,2
;----dato 2-------
    escribir msj1, len2
    leer num2,2

    mov ax, [num1]
    mov bx, [num2]
    sub ax, "0"
    sub bx, "0"
    sub ax,bx
    ;add ax, '0'
    ;mov [result], eax
    js negativo
    jmp positivo

negativo:
    escribir msj_negativo, len_negativo
    jmp salir

positivo:
    escribir msj_positivo, len_positivo
    jmp salir

salir:
    mov eax, 1
    int 80h