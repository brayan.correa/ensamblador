%macro escribir 2
    mov eax,4 
    mov ebx,1
    push cx 
    add cx, "0"
    mov [variable], cx
    mov ecx,%1
    mov edx,%2
    int 80h


%endmacro

%macro leer 2
    mov eax,3
    mov ebx,2 
    mov ecx,%1
    mov edx,%2
    int 80h 
%endmacro

section .data
    msj1 db 0x0A,"ITEM: "
    len1  equ $-msj1

section .bss
   variable resb 2

section .text
    global _start
_start:
    mov cx, 10
    
ciclo:

    cmp cx,0
    jz salir
    dec cx
    jnz imprimir_mensaje

imprimir_mensaje:
    escribir msj1, len1
    
    mov eax, 4
    mov ebx, 1
    mov ecx, variable
    mov edx, 1
    int 80h

    pop cx
    jmp ciclo

salir:
    mov eax, 1
    int 80h

