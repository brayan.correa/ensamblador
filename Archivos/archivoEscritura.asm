%macro escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro leer 2
    mov eax, 3
    mov ebx, 0
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
archivo db "/home/brayan/Documentos/ensamblador/Archivos/notas_examenes.txt",0;0 es el fin del archivo
mensaje_error db "error del archivo",10
len_mensaje equ $-mensaje_error

section .bss
    ;establecer el id del archivo 
    texto resb 35
    idarchivo resb 1 ;espacio en memoria en una entrada de datos

section .text
    global _start
_start:

    mov eax, 3
    mov ebx, 2
    mov ecx, texto
    mov edx, 35
    int 80h

    mov eax, 8
    mov ebx, archivo
    mov ecx, 1
    mov edx, 0x1FF
    int 80h

    test eax, eax
    jz error
    mov dword [idarchivo],eax

    mov eax, 4
    mov ebx, [idarchivo]
    mov ebx, texto
    mov edx, 35
    int 80h
    jmp salir

error:
    escribir mensaje_error,len_mensaje
salir:
    mov eax,1
    int 80h

