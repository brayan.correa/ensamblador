%macro escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro leer 2
    mov eax, 3
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
archivo db "/home/brayan/Documentos/ensamblador/Archivos/codigo.txt",0;0 es el fin del archivo
mensaje_error db "error del archivo",10
len_mensaje equ $-mensaje_error

section .bss
    ;establecer el id del archivo 
    texto resb 35
    idarchivo resb 1 ;espacio en memoria en una entrada de datos

section .text
    global _start
_start:
    mov eax, 5 ;para lectura de un archivo ;8 permite hacer lectura y escritura; se convoca una subrutina al sistema operativo
    mov ebx, archivo
    mov ecx, 0
                    ;modos de acceso de un archivo
                    ; R-only = 0
                    ; W-only = 1
                    ; RDWR = 2
    mov edx, 0
    int 80h

    test eax,eax ; zero = 0 significa que el archivo no existe
    jz error
    mov dword [idarchivo],eax

    mov eax, 3
    mov edx, [idarchivo]
    mov ecx, texto
    mov edx, 19
    int 80h

    escribir texto,19
    ;***************cerrar archivo***********+++++
    mov eax, 6
    mov ebx, [idarchivo]
    mov ecx, 0
    mov edx, 0
    int 80h

    jmp salir
error:
    escribir mensaje_error,len_mensaje
salir:
    mov eax,1
    int 80h

