%macro escribir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

section .data

	msj_arreglo db 10,'Arreglo: '
	len_arreglo equ $-msj_arreglo

    msj_moda db 10,'Moda: '
  	len_moda equ $ -msj_moda

	msj2 db 10, 'Archivo creado',10
	len_msj equ $- msj2

	msj3 db 10, "el mayor es: "
  	len3 equ $ -msj3

	archivoLectura db '/home/brayan/Documentos/ensamblador/archivoModaMayorMenor/datosExamen.txt',0 
	archivo db '/home/brayan/Documentos/ensamblador/archivoModaMayorMenor/moda.txt',0 
	
section .bss
	texto resb 50		;contenido del archivo
	idarchivo resd 1 	
  	repetido resb 2

section .text
	global _start

_start:
;-------------------abrir el archivo-------------------
	mov eax, 5		;5 lectura de archivo
	mov ebx, archivoLectura	;ruta
	mov ecx,0 		;modo de acceso lectura
	mov edx,0 		;permite leer si esta creado
	int 80h 

	test eax,eax		;instruccion de comparacion-->modifica el valor de las banderas
	jz salir

	mov dword[idarchivo], eax

	escribir msj_arreglo, len_arreglo

	mov eax,3		;servicio 3 lectura	
	mov ebx,[idarchivo]	;unidad de entrada
	mov ecx,texto
	mov edx,50
	int 80h

	escribir texto , 50

	mov eax,6		;6 cerrar el archivo
	mov ebx,[idarchivo]
	mov ecx,0
	mov edx,0
	int 80h
;-------------------cierra el archivo-------------------

;-----------------------recorrido---------------------------
mov esi,1                   
mov ecx,0                     
push 0

primerComaparacion:         
	mov al,[texto+ecx]     
	mov edi,esi            

segundaComparacion:        
	mov bl,[texto+edi]     
	cmp al,bl              
	je imprimir         

cadena:             
	add edi,1 	     
	cmp edi,9           
	jb segundaComparacion
	inc ecx             
	add esi,1            
	cmp ecx,9            
	jb primerComaparacion 
	jmp resultado
           
imprimir:      
	pop ecx
	mov [repetido+ecx],al
	inc ecx
	push ecx
	jmp cadena        
           
resultado:
	escribir msj_moda,len_moda       
	escribir repetido,1  


salir:

	mov eax,1
	int 80h